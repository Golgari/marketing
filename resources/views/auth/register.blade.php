@extends('layouts.app')

@section('content')
    <section class="register">

    @if(session()->has('message'))
            <div class="box">
            {{ session()->get('message') }}
            </div>
            @endif
    
        <form class="form" action="{{route('register')}}" method="post">
            @csrf
            <div class="form-item">
                <label for="name" class=""></label>
                <input type="text" name="name" id="name" placeholder="First name" aria-label="First name" class="@error('name') cool_error_message @enderror form-input" value="{{old('name')}}"> 
            </div>

                
            <div class="form-item">
                <label for="email" class=""></label>
                <input type="email" name="email" id="email" placeholder="Your Email" aria-label="Email" class="@error('email') cool_error_message @enderror form-input" value="{{old('email')}}" > 
                
            </div>

                <button type="submit" class="form-submit">SUBMIT
                </button>
            
        </form>

        @error('name')
                    <p class="alert-name">{{$message}}</p>
            @enderror

        @error('email')
                    <p class="alert-email">{{$message}}</p>
            @enderror

    </section>
@endsection