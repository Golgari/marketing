@extends('layouts.app')

@section('content')
<main class="l-main">
            <section class="home" id="home">
                <div class="home__container bd-grid">
                    <div class="home__img">
                    <img src="img/illustration.jpg" alt="" class="">
                    </div>

                    <div class="home__data">
                        <h1 class="home__title">Digital</h1>
                        <h2 class="home__subtitle">Marketing</h2>
                        <p class="home__description">Lorem ipsum dolor sit amet, consectetur adipisicing.  Vest<br> rutrum metus at enim congue scelerisque. Sed suscipit <br> non iaculis consectetur adipiscing elit. <br></p>
                        <a href="{{ route('about') }}" class="home__button">Learn More</a>
                    </div>
                </div>
            </section>
        </main>
@endsection