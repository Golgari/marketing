<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    
    <title>Landing Page</title>
</head>
<body>

    <header class="header">
    <nav class="nav bd-grid">

        <div>
            <a href="{{ route('main') }}" class="nav__logo">LOGO</a>
        </div>

        <div class="menu-btn">
            <div class="menu-btn__lines"></div>
        </div>

        <ul class="nav__list">
            <li class="nav__item"><a href="{{ route('main') }}" class="nav__link">Home</a></li>
            <li class="nav__item"><a href="{{ route('services') }}" class="nav__link">Services</a></li>
            <li class="nav__item"><a href="{{ route('about') }}" class="nav__link">About</a></li>
            <li class="nav__item"><a href="{{ route('contact') }}" class="nav__link">Contact</a></li>
            <li class="nav__item"><a href="{{ route('faq') }}" class="nav__link">FAQ</a></li>
            <li class="nav__item"><a class="nav__special" href="{{ route('register') }}">SIGN UP</a></li>
        </ul>
</div> 


    </nav>
    
    </header>
    @yield('content')    
</body>
<script src="{{asset('js/script.js')}}"></script>
</html>
