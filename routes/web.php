<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\RegistrationController;

Route::get('/register', [RegistrationController::class, 'index'])->name('register');

Route::post('/register', [RegistrationController::class, 'save']);

Route::get('/services', function () {
    return view('assets.services');
})->name('services');

Route::get('/about', function () {
    return view('assets.about');
})->name('about');

Route::get('/contact', function () {
    return view('assets.contact');
})->name('contact');

Route::get('/faq', function () {
    return view('assets.faq');
})->name('faq');

Route::get('/', function () {
    return view('main.index');
})->name('main');