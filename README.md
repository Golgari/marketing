# Digital Marketing app

A simple template for Laravel framework that includes user email registration.

## Usage

In the project directory, you can run (XAMPP is required for DBS):

```bash
php artisan migrate:fresh
php artisan serve
```

Runs the app in the development mode.
Open [http://127.0.0.1:8000](http://127.0.0.1:8000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

## Web App Features

-   Responsive (Desktop / Mobile)
-   Email sensitive to registration
-   Hamburger menu that turns in to X mark depending on a user menu usage (Open / Close)
-   Sections
-   Notification appear after registration
-   Notifications appear if user entered wrong credentials

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
