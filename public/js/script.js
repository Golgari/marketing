const menuBtn = document.querySelector(".menu-btn");
const menuItems = document.querySelector(".nav__list");
const menuItem = document.querySelectorAll(".nav__link");

menuBtn.addEventListener("click", (event) => {
    toggle();
    event.preventDefault();
});
menuItem.forEach((item) => {
    item.addEventListener("click", () => {
        if (menuBtn.classList.contains("open")) {
            toggle();
        }
    });
});

function toggle() {
    menuBtn.classList.toggle("open");
    menuItems.classList.toggle("open");
}
