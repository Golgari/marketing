<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RegistrationController extends Controller
{
    public function index() {
        return view('auth.register');
    }

    public function save(Request $request) {


        $this ->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users'
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email
        ]);

        auth()->user();


        // return redirect()->route('register');
        return back()->with('message', 'Registration is complete.');


    }
}
